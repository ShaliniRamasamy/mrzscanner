package com.hellsayenci.mrzscanner;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class UserInfoActivity extends AppCompatActivity {

    TextView textView_docNo,textView_dob,textView_sex,textView_nationality,textView_idNo,textView_surName,textView_givenName,textView_issueCountry,textView_countryCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        textView_docNo=(TextView)findViewById(R.id.textView_docNo);
        textView_dob=(TextView)findViewById(R.id.textView_dob);
        textView_sex=(TextView)findViewById(R.id.textView_sex);
        textView_nationality=(TextView)findViewById(R.id.textView_nationality);
        textView_idNo=(TextView)findViewById(R.id.textView_idNo);
        textView_surName=(TextView)findViewById(R.id.textView_surName);
        textView_givenName=(TextView)findViewById(R.id.textView_givenName);
        textView_issueCountry=(TextView)findViewById(R.id.textView_issueCountry);
        textView_countryCode=(TextView)findViewById(R.id.textView_countryCode);

        textView_docNo.setText("Document No: "+MyConstants.documentNo);
        textView_dob.setText("DOB: "+String.valueOf(MyConstants.dateOfBith));
        textView_sex.setText("Sex: "+String.valueOf(MyConstants.sex));
        textView_nationality.setText("Nationality: "+MyConstants.nationality);
        textView_idNo.setText("Id No: "+MyConstants.personalNo);
        textView_surName.setText("Surname: "+MyConstants.surName);
        textView_givenName.setText("Given name: "+MyConstants.givenName);
        textView_issueCountry.setText("Issuing country: "+MyConstants.issueingCountry);
        textView_countryCode.setText("Country code: "+MyConstants.code);

    }
}
