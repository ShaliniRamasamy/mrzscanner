package com.hellsayenci.mrzscanner;

import com.hellsayenci.mrzscanner.mrz.types.MrzDate;
import com.hellsayenci.mrzscanner.mrz.types.MrzSex;

public class MyConstants {

    public static String documentNo = null;
    public static MrzDate dateOfBith = null;
    public static MrzSex sex = null;
    public static String nationality = null;
    public static String personalNo = null;
    public static String surName = null;
    public static String givenName = null;
    public static String issueingCountry = null;
    public static String code = null;

}
