package com.hellsayenci.mrzscanner;

import com.hellsayenci.mrzscanner.mrz.types.MrzDate;
import com.hellsayenci.mrzscanner.mrz.types.MrzSex;

public class PojoForUserData {
    String documentNo;
    MrzDate dateOfBirth;
    MrzSex sex;
    String nationality;
    String personalNo;
    String surName;
    String givenName;
    String issueingCountry;
    char code1;
    char code;

    public PojoForUserData() {
    }

    public PojoForUserData(String documentNo, MrzDate dateOfBirth, MrzSex sex, String nationality, String personalNo, String surName, String givenName, String issueingCountry, char code1, char code) {
        this.documentNo = documentNo;
        this.dateOfBirth = dateOfBirth;
        this.sex = sex;
        this.nationality = nationality;
        this.personalNo = personalNo;
        this.surName = surName;
        this.givenName = givenName;
        this.issueingCountry = issueingCountry;
        this.code1 = code1;
        this.code = code;
    }

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public MrzDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(MrzDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public MrzSex getSex() {
        return sex;
    }

    public void setSex(MrzSex sex) {
        this.sex = sex;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getPersonalNo() {
        return personalNo;
    }

    public void setPersonalNo(String personalNo) {
        this.personalNo = personalNo;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getIssueingCountry() {
        return issueingCountry;
    }

    public void setIssueingCountry(String issueingCountry) {
        this.issueingCountry = issueingCountry;
    }

    public char getCode1() {
        return code1;
    }

    public void setCode1(char code1) {
        this.code1 = code1;
    }

    public char getCode() {
        return code;
    }

    public void setCode(char code) {
        this.code = code;
    }
}
